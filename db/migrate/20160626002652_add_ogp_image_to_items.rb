class AddOgpImageToItems < ActiveRecord::Migration
  def change
    add_column :items, :ogp_image, :string
  end
end
