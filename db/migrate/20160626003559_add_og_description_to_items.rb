class AddOgDescriptionToItems < ActiveRecord::Migration
  def change
    add_column :items, :og_description, :text
  end
end
