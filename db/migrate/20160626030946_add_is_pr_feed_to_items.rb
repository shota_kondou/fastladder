class AddIsPrFeedToItems < ActiveRecord::Migration
  def change
    add_column :items, :is_pr_feed, :boolean, default: false, null: false
  end
end
