class Api::ItemController < ApplicationController
  before_action :login_required_api, only: [:mark_unread]

  def mark_unread
    render_json_status(true)
  end

  def items_test
    @items = Item.where(is_pr_feed: false).page(params[:page]).per(50).order(:id)
  end
end
