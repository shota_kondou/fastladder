json.array!(@items) do |item|
  json.extract! item, :id, :link, :title, :og_description, :ogp_image, :is_pr_feed
end